<%-- 
    Document   : agregarServicio
    Created on : 18/11/2017, 10:20:26 PM
    Author     : AndresFabian
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"   %>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>añadiendoo servicio</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    </head>
    <body>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="<c:url value="/inicio.htm" />">Inicio</a></li>
                <li><a href="<c:url value="/homePropietario.htm" />">home del propietario</a></li>
                <li class="active">Registro servicio</li>
            </ol>
            <div class="panel panel-primary">
                <div class="panel-heading">Formulario de registro para servicio</div>
                <div class="panel-body">
                   
                        <form:form method="post" commandName="servicio">
                            <h1>Complete los datos</h1>
                            
                            <form:errors path="*" element="div" cssClass="alert alert-danger" />
                            
                            <p>
                                <form:label path="nombreServ">Nombre:</form:label>
                                <form:input path="nombreServ" cssClass="form-control" />
                                
                            </p>
                            
                            <p>
                        <form:label path="tipo">Tipo:</form:label>
                        <form:select path="tipo" cssClass="form-control">
                            <form:option value="0">Seleccione.....</form:option>
                            <form:options items="${tipoLista}" />
                        </form:select>
                    </p>
                            <p>
                                <form:label path="direccion">Dirección:</form:label>
                                <form:input path="direccion" cssClass="form-control" />
                            </p>
                            <p>
                                <form:label path="info">Información de contacto:</form:label>
                                <form:input path="info" cssClass="form-control" />
                            </p>
                            
                            <p>
                                <form:label path="costo">costo por Hora:</form:label>
                                <form:input path="costo" cssClass="form-control" />
                            </p>
                            <p>
                                <form:label path="horaI">hora de apertura:</form:label>
                                <form:input path="horaI" cssClass="form-control" />
                            </p>
                            <p>
                                <form:label path="horaF">hora de cierre:</form:label>
                                <form:input path="horaF" cssClass="form-control" />
                            </p>
                            
                            <hr />
                            <input type="submit" value="Enviar" class="btn btn-danger" />
                        </form:form>
                </div>
            </div>
        </div>
    </body>
</html>
