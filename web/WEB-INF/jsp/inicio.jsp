<%-- 
    Document   : inicio
    Created on : 18/11/2017, 10:26:40 AM
    Author     : AndresFabian
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>BellaColombia</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    </head>
    <body>
        <div class="container">
            <div class="row">
                <h1>BellaColombia</h1>
                <p>
                    <a href="<c:url value="verpropietario.htm" />" class="btn btn-success"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Ver Propietarios</a>
                </p>   
                <p>
                    <a href="<c:url value="agregarPropietario.htm" />" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Inscribirse como Propietario</a>
                </p> 
                <p>
                    <a href="<c:url value="sesionPropietario.htm" />" class="btn btn-success"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Iniciar Sesión Propietario</a>
                </p> 
                
            </div>
            
        </div>
    </body>
</html>
