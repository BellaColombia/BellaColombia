<%-- 
    Document   : verpropietario
    Created on : 18/11/2017, 10:32:39 AM
    Author     : AndresFabian
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"   %>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Propietarios Registrados</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    </head>
    <body>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="<c:url value="/inicio.htm" />">Inicio</a></li>
                <li class="active">Propietarios</li>
            </ol>
            <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>E-Mail</th>
                            <th>Teléfono</th>
                            <th>Fecha de Nacimiento</th>
                            <th>Username</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${data}" var="dato">
                            <tr>
                                <td><c:out value="${dato.Nombre}" /></td>
                                <td><c:out value="${dato.correo}" /></td>
                                <td><c:out value="${dato.telefono}" /></td>
                                <td><c:out value="${dato.fechaNacimiento}" /></td>
                                <td><c:out value="${dato.username}" /></td>
                                
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
        </div>
    </body>
</html>
