<%-- 
    Document   : agregarPropietario
    Created on : 18/11/2017, 02:36:29 PM
    Author     : AndresFabian
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"   %>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>registrando</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    </head>
    <body>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="<c:url value="/inicio.htm" />">Inicio</a></li>
                <li class="active">Registro Propietario</li>
            </ol>
            <div class="panel panel-primary">
                <div class="panel-heading">Formulario de registro</div>
                <div class="panel-body">
                   
                        <form:form method="post" commandName="propietario">
                            <h1>Complete los datos</h1>
                            
                            <form:errors path="*" element="div" cssClass="alert alert-danger" />
                            
                            <p>
                                <form:label path="nombre">Nombre:</form:label>
                                <form:input path="nombre" cssClass="form-control" />
                                
                            </p>
                            <p>
                                <form:label path="apellido">Apellido:</form:label>
                                <form:input path="apellido" cssClass="form-control" />
                                
                            </p>
                            <p>
                                <form:label path="correo">Correo electrónico:</form:label>
                                <form:input path="correo" cssClass="form-control" />
                            </p>
                            <p>
                                <form:label path="cedula">Cédula:</form:label>
                                <form:input path="cedula" cssClass="form-control" />
                            </p>
                            
                            <p>
                                <form:label path="telefono">Teléfono:</form:label>
                                <form:input path="telefono" cssClass="form-control" />
                            </p>
                            <p>
                                <form:label path="fechaN">Fecha de Nacimiento:</form:label>
                                <form:input path="fechaN" cssClass="form-control" />
                            </p>
                            <p>
                                <form:label path="username">Username:</form:label>
                                <form:input path="username" cssClass="form-control" />
                            </p>
                            <p>
                                <form:label path="password">Contraseña:</form:label>
                                <form:input type="password" path="password" cssClass="form-control" />
                            </p>
                            <hr />
                            <input type="submit" value="Enviar" class="btn btn-danger" />
                        </form:form>
                </div>
            </div>
        </div>
    </body>
</html>
