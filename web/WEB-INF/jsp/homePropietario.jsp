<%-- 
    Document   : homePropietario
    Created on : 18/11/2017, 07:28:50 PM
    Author     : AndresFabian
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>BellaColombia</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    </head>
    <body>
        <div class="container">
            <div class="row">
                <h1>BellaColombia Bienvenido Propietario</h1>
                <p>
                    <a href="<c:url value="agregarServicio.htm" />" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Agregar Servicio</a>
                </p>   
                
                
            </div>
            
        </div>
    </body>
</html>
