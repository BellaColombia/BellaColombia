<%-- 
    Document   : sesionPropietario
    Created on : 18/11/2017, 05:49:49 PM
    Author     : AndresFabian
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"   %>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>inicio sesión</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    </head>
    <body>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="<c:url value="/inicio.htm" />">Inicio</a></li>
                <li class="active">Inicio Sesión Propietario</li>
            </ol>
            <div class="panel panel-primary">
                <div class="panel-heading">Iniciando sesión</div>
                <div class="panel-body">
                   
                        <form:form method="post" commandName="propietario">
                            <h1>Digite su usuario y contraseña</h1>
                            
                            <form:errors path="*" element="div" cssClass="alert alert-danger" />
                            
                            <p>
                                <form:label path="username">Username:</form:label>
                                <form:input path="username" cssClass="form-control" />
                            </p>
                            <p>
                                <form:label path="password">Contraseña:</form:label>
                                <form:input type="password" path="password" cssClass="form-control" />
                            </p>
                            <hr />
                            <input type="submit" value="Entrar" class="btn btn-danger" />
                        </form:form>
                </div>
            </div>
        </div>
    </body>
</html>
