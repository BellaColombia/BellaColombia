
package com.bellacolombia.conect;

/**
 *
 * @author AndresFabian
 */
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class Conectar {
    //este es el método que permite conectarme a una base de datos MySQL cargada en el localhost
    //retorna un objeto DriverDataSource que fue el recomendado por spring framework
    public DriverManagerDataSource conectar()
    {
        DriverManagerDataSource dataSource=new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:33065/bellacolombia");
        dataSource.setUsername("root");
        dataSource.setPassword("12345");
                return dataSource;
        
    }
}