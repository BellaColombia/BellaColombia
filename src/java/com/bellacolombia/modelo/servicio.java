/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bellacolombia.modelo;

/**
 *
 * @author AndresFabian
 */
public class servicio {
     private int id, idDuenio;
    private String nombreServ,tipo, direccion,info;
    private float costo,horaI,horaF;

    public servicio() {
    }

    public servicio(int idDuenio, String nombreServ, String tipo, String direccion, String info, float costo, float horaI, float horaF) {
        this.idDuenio = idDuenio;
        this.nombreServ = nombreServ;
        this.tipo = tipo;
        this.direccion = direccion;
        this.info = info;
        this.costo = costo;
        this.horaI = horaI;
        this.horaF = horaF;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdDuenio() {
        return idDuenio;
    }

    public void setIdDuenio(int idDuenio) {
        this.idDuenio = idDuenio;
    }

    public String getNombreServ() {
        return nombreServ;
    }

    public void setNombreServ(String nombreServ) {
        this.nombreServ = nombreServ;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public float getCosto() {
        return costo;
    }

    public void setCosto(float costo) {
        this.costo = costo;
    }

    public float getHoraI() {
        return horaI;
    }

    public void setHoraI(float horaI) {
        this.horaI = horaI;
    }

    public float getHoraF() {
        return horaF;
    }

    public void setHoraF(float horaF) {
        this.horaF = horaF;
    }
    
    
    
}
