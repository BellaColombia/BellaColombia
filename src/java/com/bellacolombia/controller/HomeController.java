
package com.bellacolombia.controller;
import com.bellacolombia.conect.Conectar;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import java.util.List;

/**
 *
 * @author AndresFabian
 */
public class HomeController {
    private JdbcTemplate jdbcTemplate;
    public HomeController()
    {
        
    }
    /**
     * *
     * Método que retorna un ModelAndView, se mapea la vista que sequiere cambiar y se
     * carga a través del metodo setViewName, el parámetro debe tener el nombre de un archivo .jsp creado 
     * en la carpeta WebPages/WEB-INF/jsp
     */
    @RequestMapping("inicio.htm")
    public ModelAndView inicio()
    {
        ModelAndView mav=new ModelAndView();
        mav.setViewName("inicio");
        return mav;
    }
    
}
