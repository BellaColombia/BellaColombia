/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bellacolombia.controller;
import com.bellacolombia.conect.Conectar;
import com.bellacolombia.modelo.propietario;
import com.bellacolombia.negocio.UsuariosValidarFormato;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author AndresFabian
 */
@Controller
@RequestMapping("agregarPropietario.htm")
public class registroController {
    UsuariosValidarFormato usuariosValidar;
    private JdbcTemplate jdbcTemplate;
    public registroController() 
    {
        this.usuariosValidar=new UsuariosValidarFormato();
        Conectar con=new Conectar();
        this.jdbcTemplate=new JdbcTemplate(con.conectar() );
    }
    /*
    **método para cargar la vista, se le añade el objeto propietario
    */
    @RequestMapping(method=RequestMethod.GET) 
    public ModelAndView form()
    {
        ModelAndView mav=new ModelAndView();
        mav.setViewName("agregarPropietario");
        mav.addObject("propietario",new propietario());
        return mav;
    }
    /*
    * método para obtener los datos digitados por el usuario en la vista
    * y validar, se llama igual al de arriba porque se sobreescribe, por ende si hay errores se recarga
    *la vista, y si todo va bien  se ejecuta la sentencia sql para insertar al objeto en la base de datos
    */
     @RequestMapping(method=RequestMethod.POST)
    public ModelAndView form(@ModelAttribute("propietario") propietario p,BindingResult result,SessionStatus status)
       
    {
        this.usuariosValidar.validate(p, result);
        if(result.hasErrors())
        {
            ModelAndView mav=new ModelAndView();
            mav.setViewName("agregarPropietario");
            mav.addObject("propietario",new propietario());
            return mav;
        }else
        {
        this.jdbcTemplate.update
        (
        "insert into propietario (nombre,apellido,correo,cedula,telefono,fechaNacimiento,username,password ) values (?,?,?,?,?,?,?,?)",
         p.getNombre(),p.getApellido(),p.getCorreo(),p.getCedula(),p.getTelefono(),p.getFechaN(),p.getUsername(),p.getPassword()   );
         return new ModelAndView("redirect:/homePropietario.htm");
        }
       
    }
}
