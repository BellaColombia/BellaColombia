package com.bellacolombia.controller;
import com.bellacolombia.conect.Conectar;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import java.util.List;

/**
 *
 * @author AndresFabian
 */
public class verPcontroller {
    private JdbcTemplate jdbcTemplate;
    public verPcontroller()
    {
        Conectar conn=new Conectar();
        this.jdbcTemplate=new JdbcTemplate(conn.conectar());
    }
    /*
    método para cargar en una vista todos los propietarios registrados en la base de datos
    */
     @RequestMapping("verpropietario.htm")
    public ModelAndView verP()
    {
        ModelAndView mav=new ModelAndView();
        String sql="Select CONCAT(nombre, ' ', apellido) As Nombre, correo, telefono, fechaNacimiento, username From propietario;";
        List data=this.jdbcTemplate.queryForList(sql);
        mav.addObject("data",data);
        mav.setViewName("verpropietario");
        return mav;
    }
    
}
