/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bellacolombia.controller;
import com.bellacolombia.conect.Conectar;

import com.bellacolombia.modelo.servicio;
import com.bellacolombia.negocio.ServiciosValidarFormato;
import com.bellacolombia.negocio.UsuariosValidarFormato;
import java.util.LinkedHashMap;
import java.util.Map;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author AndresFabian
 */
@Controller
@RequestMapping("agregarServicio.htm")
public class servicioAgregarController {
    ServiciosValidarFormato serviciosValidar;
    private JdbcTemplate jdbcTemplate;
    
    public servicioAgregarController() 
    {
        this.serviciosValidar=new ServiciosValidarFormato();
        Conectar con=new Conectar();
        this.jdbcTemplate=new JdbcTemplate(con.conectar() );
    }
    @RequestMapping(method=RequestMethod.GET) 
    public ModelAndView form()
    {
        ModelAndView mav=new ModelAndView();
        mav.setViewName("agregarServicio");
        mav.addObject("servicio",new servicio());
        return mav;
    }
    @RequestMapping(method=RequestMethod.POST)
    public ModelAndView form(@ModelAttribute("servicio") servicio s,BindingResult result,SessionStatus status)
       
    {
        this.serviciosValidar.validate(s, result);
        if(result.hasErrors())
        {
            ModelAndView mav=new ModelAndView();
            mav.setViewName("agregarServicio");
            mav.addObject("servicio",new servicio());
            return mav;
        }else
        {
        this.jdbcTemplate.update
        (
        "insert into servicio (idPropietario,nombreServicio,tipo,direccion,infoContacto,costoHora,horaApertura,horaCierre ) values (?,?,?,?,?,?,?,?)",
         10,s.getNombreServ(),s.getTipo(),s.getDireccion(),s.getInfo(),s.getCosto(),s.getHoraI(),s.getHoraF() );
         return new ModelAndView("redirect:/homePropietario.htm");
        }
       
    }
    @ModelAttribute("tipoLista")
    public Map<String,String> listadoTipos()
    {
        Map<String,String> tipo=new LinkedHashMap<>();
        tipo.put("1","Restaurante");
        tipo.put("2","Gasolinera");
        tipo.put("3","Hospital");
        tipo.put("4","Punto de salud");
        tipo.put("5","Hotel");
        return tipo;
    }
}
