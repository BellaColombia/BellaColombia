/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bellacolombia.controller;
import com.bellacolombia.conect.Conectar;
import com.bellacolombia.modelo.propietario;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import java.util.List;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import com.bellacolombia.negocio.UsuariosValidarFormato;
import com.bellacolombia.negocio.ValidarSesion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Controller;


/**
 *
 * @author AndresFabian
 */
@Controller
@RequestMapping("sesionPropietario.htm")
public class sesionController {
    private JdbcTemplate jdbcTemplate;
    UsuariosValidarFormato usuariosValidar;
    ValidarSesion vs;
    public sesionController()
    {
        this.usuariosValidar=new UsuariosValidarFormato();
        this.vs=new ValidarSesion();
        Conectar con=new Conectar();
        this.jdbcTemplate=new JdbcTemplate(con.conectar());
    }
    
    @RequestMapping(method=RequestMethod.GET) 
    //public ModelAndView form(HttpServletRequest request)     {
        //ModelAndView mav=new ModelAndView();
        //int id=Integer.parseInt(request.getParameter("id"));
        //propietario datos=this.selectProp("andrescor","contraseña");
        /*String usuario=request.getParameter("usuario");
        String contraseña=request.getParameter("pass");
        propietario datos=this.selectProp(usuario,contraseña);
        if(datos!=null){
   //request.setAttribute("busuario", datos);
   request.getRequestDispatcher("homePropietario.jsp").forward(request, response);
  }else{
   PrintWriter out=response.getWriter();
   out.println("Error, no se encontro el usuario.");
  }*/public ModelAndView form()     {
        ModelAndView mav=new ModelAndView();
        mav.setViewName("sesionPropietario");
        mav.addObject("propietario",new propietario());
        return mav;
        
    }
    @RequestMapping(method=RequestMethod.POST)
    public ModelAndView form(@ModelAttribute("propietario") propietario p,BindingResult result,SessionStatus status)
       
    {
        this.usuariosValidar.validate(p, result);
         propietario datos=this.selectProp(p.getNombre(),p.getPassword());
        //int ide=this.vs.esPropietario(p.getUsername(), p.getPassword());
        //String sql="select idPropietario From propietario p where p.username='"+p.getUsername()+"' and p.password='"+p.getPassword()+"'";
        //List data=this.jdbcTemplate.queryForList(sql);
        if(result.hasErrors())
        {
            ModelAndView mav=new ModelAndView();
            mav.setViewName("sesionPropietario");
            mav.addObject("propietario",new propietario());
            return mav;
        }else
        {
            /*ModelAndView mav2=new ModelAndView();
            mav2.setViewName("homePropietario");
            mav2.addObject("id",ide);
            mav2.addObject("user",p.getUsername());
            return mav2;*/
            System.out.println(datos.getPassword()+"");
            return new ModelAndView("redirect:/inicio.htm");
        }
       
    }
    public propietario selectProp(String user, String password) 
    {
        final propietario prop = new propietario(user, password);
        String quer = "select idPropietario, username, password From propietario p where p.username='"+user+"' and p.password='"+password+"'";
        return (propietario) jdbcTemplate.query
        (
                quer, new ResultSetExtractor<propietario>() 
            {
                public propietario extractData(ResultSet rs) throws SQLException, DataAccessException {
                    if (rs.next()) {
                        prop.setId(rs.getInt("idPropietario"));
                        prop.setUsername(rs.getString("username"));
                        prop.setPassword(rs.getString("password"));
                        
                    }
                    return prop;
                }


            }
        );
    }

  
}
