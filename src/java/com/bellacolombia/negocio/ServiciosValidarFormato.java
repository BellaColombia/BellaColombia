/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bellacolombia.negocio;


import com.bellacolombia.modelo.servicio;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author AndresFabian
 */
public class ServiciosValidarFormato implements Validator {
    

   
    
    /*
    * método de interface (obligatorio*) 
    */
    @Override
    public boolean supports(Class<?> type) 
    {
        return servicio.class.isAssignableFrom(type);
    }
 /*
    * método de interface (obligatorio*) 
    * se validan los formatos, verifica si hay campos sin llenar
    * y arroja un error
    */
    @Override
    public void validate(Object o, Errors errors) {
        servicio servi=(servicio)o;
         ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nombreServ",
        "required.nombreServ", "El campo Nombre es Obligatorio.");
         ValidationUtils.rejectIfEmptyOrWhitespace(errors, "tipo",
        "required.tipo", "El campo Tipo es Obligatorio.");
         ValidationUtils.rejectIfEmptyOrWhitespace(errors, "direccion",
        "required.direccion", "El campo Dirección es Obligatorio.");
         ValidationUtils.rejectIfEmptyOrWhitespace(errors, "info",
        "required.info", "El campo información es Obligatorio.");
         ValidationUtils.rejectIfEmptyOrWhitespace(errors, "costo",
        "required.costo", "El campo Teléfono es Obligatorio.");
         ValidationUtils.rejectIfEmptyOrWhitespace(errors, "horaI",
        "required.horaI", "El campo Hora apertura es Obligatorio.");
         ValidationUtils.rejectIfEmptyOrWhitespace(errors, "horaF",
        "required.horaF", "El campo Hora cierre es Obligatorio.");
         
         
    }
}
