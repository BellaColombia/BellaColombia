/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bellacolombia.negocio;
import com.bellacolombia.modelo.propietario;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author AndresFabian
 */
public class UsuariosValidarFormato implements Validator {
    
private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
   + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
   
     private Pattern pattern;
     private Matcher matcher;
    
    @Override
    public boolean supports(Class<?> type) 
    {
        return propietario.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        propietario propietarios=(propietario)o;
         ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nombre",
        "required.nombre", "El campo Nombre es Obligatorio.");
         ValidationUtils.rejectIfEmptyOrWhitespace(errors, "apellido",
        "required.telefono", "El campo Apellido es Obligatorio.");
         ValidationUtils.rejectIfEmptyOrWhitespace(errors, "correo",
        "required.correo", "El campo Correo electrónico es Obligatorio.");
         
         if (!(propietarios.getCorreo() != null && propietarios.getCorreo().isEmpty()))
        {
            this.pattern = Pattern.compile(EMAIL_PATTERN);
            this.matcher = pattern.matcher(propietarios.getCorreo());
             if (!matcher.matches()) {
                errors.rejectValue("correo", "correo.incorrect",
                  "El Correo electrónico "+propietarios.getCorreo()+" no es válido");
               }
        }
         
         ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cedula",
        "required.cedula", "El campo cédula es Obligatorio.");
         ValidationUtils.rejectIfEmptyOrWhitespace(errors, "telefono",
        "required.telefono", "El campo Teléfono es Obligatorio.");
         ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fechaN",
        "required.fechaN", "El campo Fecha de Nacimiento es Obligatorio.");
         ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username",
        "required.username", "El campo Username es Obligatorio.");
         ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password",
        "required.password", "El campo Contraseña es Obligatorio.");
         
    }
}